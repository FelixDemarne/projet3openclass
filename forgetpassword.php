session_start();
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>accueil</title>
	<link rel="stylesheet" type="text/css" href="styleA.css">
	<link rel="stylesheet" media="screen and (min-width: 740px) and (max-width: 1280px)" href="tabletteR.css" />
	<link rel="stylesheet" media="screen and (min-width: 360px) and (max-width: 740px)" href="phoneR.css" />
</head>
<body>
	<header>
	<p><img id="logoheader" src="images/logo.png" alt="logo gbaf"></p>
</header>

	<hr class="reddivider">
	<div class="flexcenter">
		<div id="divco">
			<h2>changer de mot de passe</h2>
			<p>veuillez indiquer votre nom de compte ainsi que la réponse a votre question secrete dans les champs ci-dessous.</p>
			<form method="post" action="forgetpasswordtraitement.php" id="forgetpassword">
				<p>
					<label for="username">nom de compte :</label>
					<input type="text" name="username" id="username" required>
					
				</p>
				<p>
					<label for="usersecreter">response secrete :</label>
					<input type="password" name="usersecreter" id="usersecreter" required>
				</p>
				<input type="submit" name="forgetpasswordinput" id="forgetpasswordinput">
			</form>
		</div>
	</div>
<hr class="reddivider">
<?php include ("footer.php"); ?>
</body>
</html>