<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $_SESSION['user']['username']; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styleA.css">
	<link rel="stylesheet" media="screen and (min-width: 740px) and (max-width: 1280px)" href="tabletteR.css" />
	<link rel="stylesheet" media="screen and (min-width: 360px) and (max-width: 740px)" href="phoneR.css" />
</head>
<body>
	<?php include('header.php');?>
	<hr class="reddivider">
	<div class="flexcenter">
		<div id="infocontainer">
			<?php if (isset($_GET['error'])){echo "<strong>mauvaise réponse, veillez réessayer</strong>"; }?>
				<form method="post" action="nouveauusernametraitement.php">
					<p>
						<label for="reponceS">reponse a la question secrete :</label>
						<input type="text" name="reponceS" id="reponceS" required>
					</p>
					<p>
						<label for="nouveauusername">nouveau pseudonyme :</label>
						<input type="text" name="nouveauusername" id="nouveauusername" required>
					</p>
					<input type="submit" name="nouveauusernametraitement">
				</form>
		</div>
	</div>
	<hr class="reddivider">
	<?php include('footer.php');?>
</body>