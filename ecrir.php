<?php
session_start();
$actor_id = $_GET['id'];

try
{
	$bdd = new PDO('mysql:host=localhost;dbname=projet3;charset=utf8', 'root', '');
	$reponse = $bdd->query('SELECT * FROM acteur where id='.$actor_id );
	$actor = $reponse->fetch();
	}
catch (Exception $e)
{}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $actor['name']; ?> commentaire</title>
	<link rel="stylesheet" type="text/css" href="styleA.css">
	<link rel="stylesheet" media="screen and (min-width: 740px) and (max-width: 1280px)" href="tabletteR.css" />
	<link rel="stylesheet" media="screen and (min-width: 360px) and (max-width: 740px)" href="phoneR.css" />
</head>
<body>
	<?php include ("header.php"); ?>
	<hr class="reddivider">

	<form  id="formulairecommentaire" method="post" action="ecrirtraitement.php?id=<?php echo $actor_id; ?>">
	<p>
		vous pouvez ecrire votre commentaire ci-dessous<br />
		merci de rester courtois.<br />
		<textarea name="zonecommentaire" id="zonecommentaire" placeholder="minimum de 5 caractères" required minlength="5">
		</textarea><br />
		<input type="submit" name="envoyer">	
	</p>
	</form>

	<hr class="reddivider">
<?php include ("footer.php"); ?>
</body>
</html>
