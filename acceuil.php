<?php 
session_start();
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=projet3;charset=utf8', 'root', '');
	$reponse = $bdd->query('SELECT * FROM acteur' );

}
catch (Exception $e)
{}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>projet3</title>
	<link rel="stylesheet" type="text/css" href="styleA.css">
	<link rel="stylesheet" media="screen and (min-width: 740px) and (max-width: 1280px)" href="tabletteR.css" />
	<link rel="stylesheet" media="screen and (min-width: 360px) and (max-width: 740px)" href="phoneR.css" />
</head>
<body>
	<?php include ("header.php"); ?>
	<hr class="reddivider">
	<section id="presentationGBAF">
		<h1>qui sommes nous?</h1>
		<p>Le Groupement Banque Assurance Français​ (GBAF) est une fédération représentant les 6 grands groupes français :</p>
			<ul>
				<li>BNP Paribas</li>
				<li>BPCE</li>
				<li>Crédit Agricole</li>
				<li>Crédit Mutuel-CIC</li>
				<li>Société Générale</li>
				<li>La Banque Postale</li>	
			</ul>
		<p>Le GBAF est le représentant de la profession bancaire et des assureurs sur tousles axes de la réglementation financière française. Sa mission est de promouvoirl'activité bancaire à l’échelle nationale. C’est aussi un interlocuteur privilégié despouvoirs publics.</p>
		<p id="slot-illustration"><img id="illustration" src="images/slot-illustration.png" alt="illustration"></p>
	</section>

	<hr class="reddivider">

	<section id="presentationpartenaires">
		<h2>Acteurs et partenaires</h2>
		<div id="boxpresentation" class="borderblack3px borderradius">	
			<?php while ($actor = $reponse->fetch()){
				include('tata.php');
			} ?>
		</div>
	</section>
	<hr class="reddivider">
	<?php include ("footer.php"); ?>
</body>
</html>